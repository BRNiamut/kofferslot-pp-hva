package Models;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bastiaanniamut
 */
public class Letter {

    private char letter;

    public Letter() {
        this.letter = 'A';
    }

    public char getLetter() {
        return this.letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public boolean volgende() {
        this.letter++;
        if (this.getLetter() > 'Z') {
            this.setLetter('A');
            return true;
        }
        return false;
    }
}
