/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * @author bastiaanniamut
 */
public class Cijfer {
    private int cijfer;

    public Cijfer() {
        this.cijfer = 0;
    }
    
    public void setCijfer(int cijfer){
        this.cijfer = cijfer;
    }
    
    public int getCijfer(){
        return this.cijfer;
    }
    
    public boolean volgende(){
        this.cijfer++;
        if(this.cijfer > 9){
            this.cijfer = 0;
            return true;
        }
        return false;
    }
    
    
}
