package kofferslot;

import Models.Cijfer;
import Models.Kofferslot;
import Models.Letter;

/**
 *
 * @author bastiaanniamut
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        
        Kofferslot kofferslot1 = new Kofferslot('A', 'A', 0);
        Kofferslot kofferslot2 = new Kofferslot('B', 'R', 9);
        Kofferslot kofferslot3 = new Kofferslot('D', 'Z', 9);
        Kofferslot kofferslot4 = new Kofferslot('Z', 'Z', 9);
        
        kofferslot1.volgende();
        kofferslot2.volgende();
        kofferslot3.volgende();
        kofferslot4.volgende();
        
        System.out.println(kofferslot1.getCombinatie());
        System.out.println(kofferslot2.getCombinatie());
        System.out.println(kofferslot3.getCombinatie());
        System.out.println(kofferslot4.getCombinatie());
        
        
    }
    
}
