/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

import java.util.ArrayList;

/**
 *
 * @author bastiaanniamut
 */
public class Kofferslot {
    private ArrayList<Letter> letters = new ArrayList<>();
    private Cijfer cijfer = new Cijfer();;

    public Kofferslot() {  
       for (int i = 0; i < 2; i++) {
            Letter letter = new Letter();
            this.letters.add(letter);
        } 
    }
    
    public Kofferslot(char letter1, char letter2, int cijfer){
       Letter letterOne = new Letter();
       letterOne.setLetter(letter1);
       
       Letter letterTwo = new Letter();
       letterTwo.setLetter(letter2);
       
       this.letters.add(letterOne);
       this.letters.add(letterTwo);
    
       this.cijfer.setCijfer(cijfer);    
    }   
    
    public void volgende(){
       if(cijfer.volgende()){
           if(letters.get(1).volgende()){
               letters.get(0).volgende();
           }
       }
    }
    
    public String getCombinatie(){
        return String.valueOf(letters.get(0).getLetter()) + String.valueOf(letters.get(1).getLetter()) + cijfer.getCijfer() + "";
    }
}
